import requests

from flask import current_app, request

from functools import wraps

from exceptions import BadRequest


def _validate_app_secret_key():

    secret_key = request.headers.get('X-Api-Key', None)

    if not secret_key == current_app.config['APP_SECRET_KEY']:
        raise BadRequest("Forbidden Access", 403, 1)


def _validate_jwt_user():

    jwt = request.headers.get('Authorization', None)

    if not jwt:
        raise BadRequest("Forbidden Access", 403, 1)
    url = current_app.config["USER_API"] + "/user/check"
    headers = {
        'Authorization': jwt,
        'x-api-key': current_app.config["APP_SECRET_KEY"]
    }
    r = requests.get(url, headers=headers)
    if r.status_code != 200:
        raise BadRequest("Something Happens", 200, 1)

    user_data = r.json()

    if not user_data:
        raise BadRequest("User Not Found", 200, 1)

    user_id = user_data["data"]["user_id"]

    return user_id


def secret_key_required():
    def wrapper(func):
        @wraps(func)
        def decorator(*args, **kwargs):
            _validate_app_secret_key()
            return func(*args, **kwargs)

        return decorator

    return wrapper


def jwt_check(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        user_id = _validate_jwt_user()
        result = f(user_id, *args, **kwargs)
        return result

    return wrapper
