from flask import Blueprint
from flask import jsonify

from helpers import Helper

helper = Helper()

bp = Blueprint(__name__, 'index')


@bp.route("/is_alive", methods=["GET"])
def is_alive():
    # success response format
    response = {"error": 0, "message": "Connected", "data": ""}

    return jsonify(response)
