FROM python:3.5

# INSTALL PYTHON DEPEDENCIES
ADD requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

# COPY APPLICATION & SET WORKING DIRECTORY
COPY . /src
WORKDIR /src

# EXPORT PORT
EXPOSE 6000

# RUN APP
CMD ["python", "wsgi.py", "--port=6000"]
