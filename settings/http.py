import logging.config
import routes

from flask import Flask
from flask_compress import Compress
from flask_cors import CORS

from raven.contrib.flask import Sentry

from flask_socketio import SocketIO

from helpers import mongo

compress = Compress()
cors = CORS()
sentry = Sentry()
socketio = SocketIO(async_mode='threading', ping_timeout=300000)


def create_app(configuration):
    app = Flask(
        __name__.split(',')[0],
        static_url_path='/static',
        static_folder='../static')

    # Register route blueprint
    app.register_blueprint(routes.index.bp)
    app.register_blueprint(routes.error.bp)

    # load configuration
    app.config.from_object(configuration)

    logging.config.dictConfig(configuration.LOG_CONFIGURATION)

    # flask extensions initialization
    # mongo init
    mongo.init_app(app)

    compress.init_app(app)
    cors.init_app(app, resources={r"/*": {"origins": "*"}})
    sentry.init_app(app, dsn=app.config['SENTRY_DSN'])
    app.sentry = sentry
    socketio.init_app(app)

    return app
