import os
from datetime import timedelta

from dotenv import load_dotenv, find_dotenv
from pymongo.read_preferences import ReadPreference

# Load environment
load_dotenv(find_dotenv())

# get absolute path static directory in root project
log_folder = os.path.abspath(os.path.join(
    os.path.dirname(os.path.dirname(__file__)), 'log'))

# logging formatter based on
# https://docs.python.org/2/howto/logging-cookbook.html#an-example-dictionary-based-configuration
logconfiguration = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "[%(levelname)s] %(asctime)s - %(name)s - %(filename)s:%(lineno)d: %(message)s",
            'datefmt': "%Y-%m-%d %H:%M:%S"
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'verbose',
            'stream': 'ext://sys.stdout'
        },
        'logstash': {
            'level': 'DEBUG',
            'class': 'logstash.TCPLogstashHandler',
            'host': 'log.iandkenzt.com',
            'port': 5044,  # Default value: 5959
            'version': 1,  # Version of logstash event schema. Default value: 0 (for backward compatibility of the library)
            'message_type': 'logstash',  # 'type' field in logstash message. Default value: 'logstash'.
            'fqdn': False,  # Fully qualified domain name. Default value: false.
            'tags': [os.getenv("LOG_TAGS", "api-python-bp")],  # list of tags. Default: None.
        },
    },
    'loggers': {},
    'root': {
        'level': 'DEBUG',
        'handlers': ['logstash', 'console']
    }
}


class Configuration(object):
    # Basic
    DEBUG = os.getenv("DEBUG") == "True"
    PORT = int(os.getenv("PORT", 5000))

    # Mongo configuration
    MONGO_URI = os.getenv("MONGODB_CONN_STRING", "mongodb://localhost:27017/boilerplate")

    MONGO_READ_PREFERENCE = ReadPreference.SECONDARY_PREFERRED

    APP_SECRET_KEY = os.getenv("APP_SECRET_KEY")

    SENTRY_DSN = os.getenv("SENTRY_DSN")

    LOG_TAGS = os.getenv("LOG_TAGS")

    EMAIL_HOST = os.getenv("EMAIL_HOST")

    # Log
    LOG_CONFIGURATION = logconfiguration
    LOG_FOLDER = os.getenv("LOG_FOLDER", log_folder)
