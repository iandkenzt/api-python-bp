# API-PYTHON-BP #

API python boilerplate is an Rest API service that uses HTTP requests to GET, PUT, POST, and DELETE data which develop using Python (version 3) and Flask.

# Installation #

This section how to running using native python (using virtual environment) and Docker. Before setup installation please install virtualenv and Docker.

## Setup Application & Environment ##
1) clone repository

2) change directory to repository
  
3) create virtual environment:
> `virtualenv -p python3.5 env --no-site-packages`

4) install requirements:
> `source env/bin/activate && pip install -r requirements.txt`
  
5) Copy .env.example to .env and edit accordingly

## Run Application ##

This section how to running using native python and Docker.

### Native ###

1) activate virtualenv:
> `source env/bin/activate`

2) Change directory to application path:
> `python run.py`

### Docker ###

1) Docker Build
> `docker build -t api-python-bp:master .`

2) Run With Docker Compose
> `docker-compose up`

## Development ##

This section explain how to Developers used boilerplate:

### Configure git pre-commit ###

1) Go to and copy script
> `https://github.com/google/yapf/blob/master/plugins/pre-commit.sh`

2) Create pre-commit file and paste code script
> `vim .git/hooks/pre-commit`

3) Give permission
> `chmod 777 .git/hooks/pre-commit`

## Endpoint ##

This section lists all of the API Python boilerplate endpoints:

Method | URI path | Header |Description |
--- | --- | --- | --- |
GET | **[/is alive](http://localhost:5002/is_alive)** | content-type | For check status server |