#!/usr/bin/env bash

DI=${1}
REPL=${2}

export DOCKER_IMAGES=${DI}
export REPLICAS=${REPL}

sed -e "s/DOCKER_IMAGES/$DOCKER_IMAGES/g" -e "s/REPLICAS/$REPLICAS/g" ./manifests/deployment.yml > ./manifests/.deployment.yml

kubectl apply -f ./manifests/.deployment.yml
kubectl apply -f ./manifests/service.yml
